# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      <home-manager/nixos>
    ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  # Support for new hardware
  #boot.loader.efi.efiSysMountPoint = "/boot/efi";
  #boot.kernelPackages = pkgs.linuxPackagesFor (pkgs.linuxKernel.packages.linux_5_19_hardened.kernel);
  boot.kernelPackages = pkgs.linuxPackages_latest;
  boot.kernelModules = [ "kvm-intel" ];

  networking.hostName = "thuncpad"; # Define your hostname.
  # Pick only one of the below networking options.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.
  networking.networkmanager.enable = true;  # Easiest to use and most distros use this by default.

  # Set your time zone.
  time.timeZone = "Europe/Copenhagen";

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";
  console = {
    font = "Lat2-Terminus16";
    keyMap = "dk-latin1";
  };

  # Enable the X11 windowing system.
  services.xserver.enable = true;


  # Enable the GNOME Desktop Environment.
  services.xserver.displayManager.gdm.enable = true;
  services.xserver.desktopManager.gnome.enable = true;
  services.xserver.wacom.enable = true; # touch screen and pen stuff
  hardware.sensor.iio.enable = true; # enable accelerometer for autorotate
  environment.gnome.excludePackages = (with pkgs; [
    gnome-tour
  ]) ++ (with pkgs.gnome; [
    gnome-music
    gnome-terminal
    gedit
    epiphany
    geary
    evince
    tali
    iagno
    hitori
    atomix
  ]);

  # Configure keymap in X11
  services.xserver.layout = "dk";
  services.xserver.xkbOptions = "caps:escape";

  # Enable CUPS to print documents.
  services.printing = {
    enable = true;
    drivers = [
      pkgs.gutenprint
      pkgs.gutenprintBin
      pkgs.hplip
      pkgs.hplipWithPlugin
    ];
  };

  # Enable sound.
  sound.enable = true;
  hardware.pulseaudio.enable = true;
  hardware.bluetooth.enable = true;

  # Enable touchpad support (enabled default in most desktopManager).
  services.xserver.libinput.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  nixpkgs.config.allowUnfree = true;
  users.users.pebble = {
    isNormalUser = true;
    extraGroups = [ "wheel"
                    "libvirtd"
                    "docker" ]; # Enable ‘sudo’ for the user.
    packages = with pkgs; [
      firefox
      chromium
      spotify
      discord
      gimp
      krita
      inkscape
      zathura
      gh
      zoom-us
      fprintd
      fprintd-tod
      python311
      teams
      xournal
    ];
  };
  fonts.fonts = with pkgs; [
    fira-code
    fira
    fira-mono
    fira-code-symbols
    jetbrains-mono 
    noto-fonts-cjk
    noto-fonts-emoji
    liberation_ttf
  ];
  services.fprintd.enable = true;
  virtualisation = { 
    libvirtd.enable = true;
    docker.enable = true;
  };
  programs.dconf.enable = true;
  home-manager.users.pebble = {pkgs, ...}: {
    programs.bash = {
      enable = true;
      initExtra = ''export PS1="\[\e[31m\][\[\e[m\]\[\e[33m\]\u\[\e[m\]\[\e[32m\]@\[\e[m\]\[\e[34m\]\h\[\e[m\] \[\e[35m\]\W\[\e[m\]\[\e[31m\]]\[\e[m\]\\$ "
      set -o vi'';
    };
    # Home Manager needs a bit of information about you and the
    # paths it should manage.
    home.username = "pebble";
    home.homeDirectory = "/home/pebble";

    home.packages = [
      pkgs.poppler_utils
      pkgs.htop
      pkgs.texlive.combined.scheme-full
      pkgs.clisp
      pkgs.alacritty
      pkgs.libreoffice
      pkgs.clang
      pkgs.clang-tools
      pkgs.docker-client
      pkgs.cargo
      pkgs.rustc
      pkgs.rust-analyzer
      pkgs.rustfmt
      pkgs.cabal-install
    ];

    # This value determines the Home Manager release that your
    # configuration is compatible with. This helps avoid breakage
    # when a new Home Manager release introduces backwards
    # incompatible changes.
    #
    # You can update Home Manager without changing this value. See
    # the Home Manager release notes for a list of state version
    # changes in each release.
    home.stateVersion = "22.05";

    # Let Home Manager install and manage itself.
    programs.home-manager.enable = true;

    programs.emacs = {
      enable = true;
      extraPackages = epkgs: [
        epkgs.nix-mode
      ];
    };
    programs.git = {
      enable = true;
      userName = "Rasmus Hansen";
      userEmail = "rasmus14326@gmail.com";
    };
  };
  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    vim 
    wget
    ghc
    clang
    neofetch
    virt-manager
    xf86_input_wacom
    libnotify
    adoptopenjdk-icedtea-web
    wineWowPackages.stable
    winetricks
    gnumake
    gnuplot
    glfw
    cmake
    lldb
    llvm
    pkg-config
    libmysqlclient
    apacheHttpd
    docker
    vscode
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Open ports in the firewall.
  networking.firewall.allowedTCPPorts = [ 57621 ]; # spotify
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # Copy the NixOS configuration file and link it from the resulting system
  # (/run/current-system/configuration.nix). This is useful in case you
  # accidentally delete configuration.nix.
  # system.copySystemConfiguration = true;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "22.05"; # Did you read the comment?

}

